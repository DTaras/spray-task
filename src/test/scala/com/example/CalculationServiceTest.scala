package com.example

import java.io.File

import com.example.controllers.PostParams
import com.example.services.CalculateService
import com.google.inject.Inject
import com.typesafe.config.{Config, ConfigFactory}
import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest

import scala.io.Source

/**
  * Created by dtaras on 01.03.17.
  */
class CalculationServiceTest @Inject()(calculationService: CalculateService)
  extends Specification
    with Specs2RouteTest {

  val testValue = 1
  val config: Config = ConfigFactory.load()

  lazy val file1: String = config.getString("app.path.to.file1")
  lazy val file2: String = config.getString("app.path.to.file2")

  def getValFromFile(file: File, v: Int): Unit = {
    try {
      val iter = for (line <- Source.fromFile(file).getLines()) yield {
        val res = line.split(",").map(_.trim)
        res(0).toInt
      }
      val len = iter.length
      iter.toList(len)
    } catch {
      case ex: Exception => -1000
    }
  }

  "CalculationService" should {

    "readCurrentNumberFromFile" in {
      val res = calculationService.readCurrentNumberFromFile(testValue)
      res mustEqual 2
    }

    "calculation" in {
      val res = calculationService.calculation(2,3,4)
      res mustEqual 0
    }
    "putAnswerInF2" in {
      val testVal = 4567987
      val file = new File(file2)
      calculationService.putAnswerInF2(testVal)
      val check = getValFromFile(file, testVal)
      check mustNotEqual -1000
      check mustEqual testVal
    }
  }

}
