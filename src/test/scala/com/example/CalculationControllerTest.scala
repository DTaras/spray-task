package com.example

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._
import com.example.controllers.{CalculateController, PostParams}

/**
  * Created by dtaras on 25.02.17.
  */
class CalculationControllerTest extends Specification with Specs2RouteTest with CalculateController{
  def actorRefFactory = system

  "CalculationController" should {

    "Get" in {
      Get("/rest/calck?v1=0") ~> myRoute ~> check {
        response.status mustEqual OK
        response.entity.data.asString mustEqual "<res>1</res>"
      }
    }

    "Post" in {
      import com.example.controllers.PostParamsImpl._
      import spray.httpx.SprayJsonSupport._
      Post("/rest/calck", PostParams(v2=1, v3=2, v4=3)) ~> sealRoute(myRoute) ~> check {
        response.status mustEqual OK
        response.entity.data.asString mustEqual "<res>0</res>"
      }
    }

    "Post" in {
      import com.example.controllers.PostParamsImpl._
      import spray.httpx.SprayJsonSupport._
      Post("/calck", PostParams(v2=1, v3=2, v4=3)) ~> sealRoute(myRoute) ~> check {
        response.status mustEqual NotFound
      }
    }
  }
}
