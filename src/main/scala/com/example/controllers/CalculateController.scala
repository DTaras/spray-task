package com.example.controllers

import akka.actor.Actor
import com.example.services.{CalculateService, CalculateServiceImpl}
import spray.http.MediaTypes._
import spray.json.DefaultJsonProtocol
import spray.routing._


case class PostParams(v2: Int, v3: Int, v4: Int)

object PostParamsImpl extends DefaultJsonProtocol {
  implicit val json = jsonFormat3(PostParams)
}

/**
  * Created by dtaras on 25.02.17.
  */
class CalculateControllerActor extends Actor with CalculateController {

  def actorRefFactory = context

  def receive = runRoute(myRoute)
}

trait CalculateController extends HttpService {

  lazy val service = new CalculateServiceImpl

  val myRoute =
    path("rest" / "calck") {
      import PostParamsImpl._
      import spray.httpx.SprayJsonSupport._
      get {
        parameters('v1.as[Int]) {
          v1 =>
            respondWithMediaType(`text/xml`) {
              val result: Int = service.readCurrentNumberFromFile(v1)
              complete(<res>{result}</res>)
            }
        }
      } ~ post {
        entity(as[PostParams]) { param =>
          respondWithMediaType(`text/xml`) {
            val result: Int = service.calculation(param.v2, param.v3, param.v4)
            complete(<res>{result}</res>)
          }
        }
      }
    }
}