package com.example.services

import java.io.FileWriter

import com.typesafe.config.{Config, ConfigFactory}

import scala.io.Source

/**
  * Created by dtaras on 25.02.17.
  */

trait CalculateService {

  def readCurrentNumberFromFile(v1: Int): Int

  def calculation(v2: Int, v3: Int, v4: Int): Int

  def putAnswerInF2(answer: Int): Unit

  def getCurrentFromFile(file: String, v: Int): Int
}

class CalculateServiceImpl extends CalculateService{

  val config: Config = ConfigFactory.load()

  lazy val file1: String = config.getString("app.path.to.file1")
  lazy val file2: String = config.getString("app.path.to.file2")

  override def getCurrentFromFile(file: String, v: Int): Int = {
    try {
      val iter = for (line <- Source.fromFile(file).getLines()) yield {
        val res = line.split(",").map(_.trim)
        res(0).toInt
      }
      val stream = iter.toStream
      stream.take(v).last
    } catch {
      case ex: Exception => ex.printStackTrace(); throw ex
    }
  }

  override def readCurrentNumberFromFile(v1: Int): Int = {
    try {
      val res = getCurrentFromFile(file1, v1+1)
      if (res > 10) res - 10
      else res
    } catch {
      case ex: Exception =>ex.printStackTrace(); throw ex
    }
  }

  override def calculation(v2: Int, v3: Int, v4: Int): Int = {
    try {
      val res1 = getCurrentFromFile(file1, v3+1)
      if ((res1 + v2) < 10) {
        putAnswerInF2(res1 + v2 + 10); 0
      }
      else {
        putAnswerInF2(res1 + v2); 1
      }
    } catch {
      case ex: Exception =>
        ex.printStackTrace(); throw ex
    }
  }

  override def putAnswerInF2(answer: Int): Unit = {
    val fw = new FileWriter(file2, true)
    try {
      fw.write(",\n"+answer.toString)
    }
    catch {
      case ex: Exception => ex.printStackTrace()
    }
    finally fw.close()
  }

}